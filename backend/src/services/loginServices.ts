import bcryptjs from 'bcryptjs';
import nodemailer from 'nodemailer';

import config from '../config';
import { Login, User } from '../db';

export const sendVerifyMail = async (mail: string, token: string): Promise<void> => {
    const link = `${config.host}/login/mail?token=${token}`;

    const client = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        tls: {
            
        },
        auth: {
            user: config.mail,
            pass: config.mailServiceKey,
        }
    });

    await client.sendMail({
        to: mail,
        from: config.mail,
        subject: 'Email Confirmation',
        text: 'Hello! Click the link below to finish signin process.\r\n\r\n' + link,
        html: `<p>Hello! Click the link below to finish signin process.<a href="${link}">Link</p>`
    });
}

export const hashPassword = (password: string): string => {
    const salt = bcryptjs.genSaltSync();
    return bcryptjs.hashSync(password, salt);
}

export const login = (req: Express.Request, user: Express.User, callback: (e: any) => void) => {
    req.logIn(user, (e) => {
        if (e) throw e;
        const record = new Login({
            sid: req.session.id,
            userId: (user as User).id,
        });
        record.save();
        callback(e);
    });
}
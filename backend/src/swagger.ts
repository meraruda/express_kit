import swaggerAutogen from 'swagger-autogen';

import config from './config';

swaggerAutogen('./swagger_output.json', ['./index.ts'], {
  host: '',
  basePath: config.isDev ? '' : '/api',
  schemes: config.isDev ? ['http'] : ['https'],
});

import express from 'express';
import strongErrorHandler from 'strong-error-handler';
import passport from 'passport';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import SessionStore from 'connect-session-sequelize';
import flash from 'connect-flash';
import { haven } from 'domain-haven';
import Swagger from 'swagger-ui-express';

import SwaggerDocument from './swagger_output.json';

import { router, sessionRouter } from './route';
import config from './config';
import { sequelize } from './db';

require('express-async-errors');

const app = express();

const connectDatabase = (async () => {
  try {
    await sequelize.authenticate();
    console.info('Connection to database has been established successfully.');
    await sequelize.sync({ force: false });
    console.info('Sync database schema successfully.');
  } catch (err) {
    console.error('Unable to connect to the database:', err);
    if (!config.isDev) throw err;
  }
})();

app.use('/doc', Swagger.serve, Swagger.setup(SwaggerDocument));

app.use(express.urlencoded({
  extended: true,
}));
app.use(express.json());
app.use(cookieParser());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', config.frontend);
  res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, PATCH, GET, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Api-Key, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Credentials', 'true');

  next();
});

app.use(router);

const store = new (SessionStore(session.Store))({
  db: sequelize,
  table: 'Sessions',
  extendDefaultFields: (defaults, _session) => ({
    userId: _session.passport?.user,
  }),
});

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  store,
  cookie: {
    httpOnly: false,
  },
}));

app.use(passport.initialize());
app.use(flash());
app.use(passport.session());

app.use(sessionRouter);

app.use(haven());

app.use(strongErrorHandler({ debug: config.isDev }));

connectDatabase.then(() => {
  console.info(`Running in ${config.isDev ? 'Development' : 'Production'} mode`);
  app.listen(8080);
});

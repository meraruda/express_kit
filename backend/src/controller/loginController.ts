import { google } from 'googleapis';
import { Request, Response, NextFunction, Express } from "express";
import jwtToken from 'jsonwebtoken';
import { body, query } from 'express-validator';

const Facebook = require("facebook-js-sdk");

import config from '../config';
import { User, sequelize } from '../db';
import passport from '../authorization';
import { sendVerifyMail, hashPassword, login } from '../services/loginServices';
import validator from '../validator';
import { IVerifyOptions } from 'passport-local';


export default class loginController {
    static createGoogleAuthObj() {
        return new google.auth.OAuth2(
            config.googleClientID,
            config.googleSecret,
            config.googleRedirect
        );
    }

    static createFacebookAuthObj() {
        return new Facebook({
            appId: config.facebookAppID,
            appSecret: config.facebookSecret,
            redirectUrl: `${config.host}/signin-facebook`,
            graphVersion: "v17.0",
          });
    }

    static loginWithGoogle = async (req: Request, res: Response) => {
        // #swagger.tags = ['Login']
        /* #swagger.responses[200] = {
            schema: {
                url: 'string'
            }
         }  */

        const auth = loginController.createGoogleAuthObj();

        const url = auth.generateAuthUrl({
            access_type: 'offline',
            prompt: 'consent',
            scope: [
                'https://www.googleapis.com/auth/plus.me',
                'https://www.googleapis.com/auth/userinfo.email',
                'https://www.googleapis.com/auth/userinfo.profile'
            ]
        });

        return res.status(200).json({
            url
        });
    }

    static handleGoogleRedirect = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']
        /*
             #swagger.parameters['code'] = {
                in: 'query',
                required: true,
                type: 'string'
            }
        */

        await validator([
            query('code').isLength({ min: 1 }),
        ])(req, res, async () => {
            const auth = loginController.createGoogleAuthObj();
            const { code }: any = req.query;
            auth.setCredentials((await auth.getToken(code)).tokens);

            const oauth2 = google.oauth2({
                auth,
                version: 'v2'
            });

            const info = await oauth2.userinfo.get();
            const [user, created] = await User.findOrCreate({
                where: { mail: info.data.email },
                defaults: {
                    mail: info.data.email,
                    name: info.data.name,
                    valid: true,
                }
            })

            login(req, user, () => res.redirect(config.frontend));
        })
    }

    static loginWithFacebook = async(req: Request, res: Response) => {
        // #swagger.tags = ['Login']
        /* #swagger.responses[200] = {
            schema: {
                url: 'string'
            }
         }  */

         const auth = loginController.createFacebookAuthObj();

         const url = auth.getLoginUrl(["email"]);
         
         return res.status(200).json({
            url
        });
    }

    static handleFaceBookRedirect = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']
        /*
             #swagger.parameters['code'] = {
                in: 'query',
                required: true,
                type: 'string'
            }
        */

        await validator([
            query('code').isLength({ min: 1 }),
        ])(req, res, async () => {
            const auth = loginController.createFacebookAuthObj();
            const { code }: any = req.query;
       
            const response = await auth.callback(code);
            const { access_token } = response.data;

            auth.setAccessToken(access_token);

            const { data } = await auth.get("/me?fields=id,name,email");

            const [user, created] = await User.findOrCreate({
                where: { mail: data.email },
                defaults: {
                    mail: data.email,
                    name: data.name,
                    valid: true,
                }
            })

            login(req, user, () => res.redirect(config.frontend));
        })
    }

    static login = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']
        /*
             #swagger.parameters['login'] = {
                in: 'body',
                required: true,
                schema: {
                    username: "mail@mail.account",
                    password: "password"
                }
            }
        */

        await validator([
            body('username').isEmail(),
            body('password').isLength({ min: 8 })
        ])(req, res, () => passport.authenticate('local', async (e: Error, user: Express.User, valid: IVerifyOptions) => {
            if (e) return res.status(500).json({ message: e })
            if (valid?.message) return res.status(401).json({ message: valid.message })
            if (!user) return res.status(401).json({ message: 'User not found' })
            if (!(user as User).valid) return res.status(307).json({ message: 'Please valid your email' })
            login(req, user, () => res.status(200).json({ message: 'done' }));
        })(req, res, next));
    }

    static logout = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']
        req.logOut((e) => {
            if (e) {
                return next(e);
            }

            res.status(200).json({ message: 'done' });
        });
    }

    static resentVerifyMail = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']

        await validator([
            body('mail').isEmail(),
        ])(req, res, async () => {
            const { mail } = req.body

            const user = await User.findOne({ where: { mail } });
            if (!user) return res.status(401).json({ message: 'User not found' })

            const token = jwtToken.sign({ mail }, config.host);

            await sendVerifyMail(mail, token);

            res.status(200).json({
                message: 'done'
            });
        });
    }

    static verifyMail = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']
        /*
             #swagger.parameters['token'] = {
                in: 'query',
                required: true,
                type: 'string'
            }
        */

        await validator([
            query('token').isJWT(),
        ])(req, res, async () => {
            const token = req.query.token?.toString();
            if (token && jwtToken.verify(token, config.host)) {
                const mail = jwtToken.decode(token);
                const user = await User.findOne({ where: { mail } });
                if (user) {
                    user.update({ valid: true });
                    login(req, user, () => res.redirect(config.frontend));
                }
            }
        });
    }

    static register = async (req: Request, res: Response, next: NextFunction) => {
        // #swagger.tags = ['Login']
        /*
          #swagger.parameters['register'] = {
                in: 'body',
                required: true,
                schema: {
                    username: "name",
                    mail: "mail@mail.account",
                    password: "P@ssword"
                }
            } */
        await validator([
            body('username').isLength({ min: 1 }),
            body('mail').isEmail(),
            body('password').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{}[\]|;:'",<.>/?]).{8,}$/)
        ])(req, res, async () => {
            const userData = { name: req.body.username, mail: req.body.mail }

            const token = jwtToken.sign(userData.mail, config.host);

            const [user, created] = await User.findOrCreate({
                where: { mail: userData.mail },
                defaults: {
                    mail: userData.mail,
                    name: userData.name,
                    valid: true,
                }
            })

            await sendVerifyMail(user.mail, token);

            res.status(200).json({
                message: 'done'
            });
        })
    }
}
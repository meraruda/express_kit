import { Request, Response } from "express"

import { Sessions, User, sequelize } from '../db';
import { hashPassword } from '../services/loginServices';
import { Op } from "sequelize";
import { body, query } from 'express-validator';

import validator from '../validator';

export default class userController {
    static async getUser(req: Request, res: Response) {
        // #swagger.tags = ['User']
        /* #swagger.responses[200] = {
            schema: {
                name: '',
                mail: ''
            }
         }  */

        const { name, mail } = req.user as User;
        res.status(200).json({
            name,
            mail
        });
    }

    static async updateUser(req: Request, res: Response) {
        // #swagger.tags = ['User']

        await validator([
            body('name').isLength({ min: 1 }),
        ])(req, res, async () => {

            const { name } = req.body;
            const { id } = req.user as User;
            await User.update(
                {
                    name
                },
                {
                    where: {
                        id
                    }
                });

            return res.status(200).json({
                message: 'done'
            });
        })
    }

    static async resetPassword(req: Request, res: Response) {
        // #swagger.tags = ['User']
        /*
          #swagger.parameters['password'] = {
                in: 'body',
                required: true,
                schema: {
                    password: "P@ssword"
                }
            }
        */
        /* #swagger.responses[200]
         }  */

        await validator([
            body('password').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{}[\]|;:'",<.>/?]).{8,}$/)
        ])(req, res, async () => {
            const { password } = req.body;
            const { id } = req.user as User;
            await User.update(
                {
                    password: hashPassword(password),
                },
                {
                    where: {
                        id
                    }
                });
            return res.status(200).json({
                message: 'done'
            });
        })
    }

    static async getUserList(req: Request, res: Response) {
        // #swagger.tags = ['User']
        /* #swagger.responses[200] = {
            schema: [{
                name: '',
                mail: '',
                createdAt: '1999-01-01T00:00:00.000Z',
                times: '',
                last: '1999-01-01T00:00:00.000Z',
            }]
         }  */

        const { id } = req.user as User;
        const [results] = await sequelize.query(`
        SELECT "name", "mail", "createdAt", "times", "last"
        FROM "Users" 
        LEFT JOIN 
            (SELECT count(1) AS times, "userId" FROM "Logins" GROUP BY "userId") AS "login"
            ON "Users"."id" = "login"."userId"
        LEFT JOIN 
            (SELECT MAX("createdAt") AS "last", "userId" FROM "Sessions" GROUP BY "userId") AS "session" 
            ON "Users"."id" = "session"."userId"
        WHERE id = :id`, {
            replacements: { id },
        });
        return res.status(200).json(results);
    }

    static async getUserStatistic(req: Request, res: Response) {
        // #swagger.tags = ['User']
        /* #swagger.responses[200] = {
            schema: {
                total: 0,
                active: 0,
                week: 0
            }
         }  */

        try {
            const total = await User.count();
            const active = await Sessions.count({
                where: {
                    createdAt: {
                        [Op.gt]: new Date().setHours(0, 0, 0, 0),
                        [Op.lt]: new Date()
                    },
                    userId: {
                        [Op.ne]: null
                    }
                },
            })
            const week = await Sessions.count({
                where: {
                    createdAt: {
                        [Op.gt]: new Date(new Date().getDate() - 7).setHours(0, 0, 0, 0),
                        [Op.lt]: new Date()
                    },
                },
            })

            return res.status(200).json({
                total,
                active,
                week
            });
        } catch (e) {
            console.debug(e);
        }
    }

    static async deleteUser(req: Request, res: Response) {
        // #swagger.tags = ['User']
        /* #swagger.responses[200] = {
         }  */

         const { id } = req.user as User;
         await User.destroy(
             {
                 where: {
                     id
                 }
             });
         return res.status(200).json({
             message: 'done'
         });
    }

    static async uploadAvatar(req: Request, res: Response) {
        // #swagger.tags = ['User']
        /*
          #swagger.consumes = ['multipart/form-data']  
          #swagger.parameters['avatar'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
        } */
        /* #swagger.responses[200] = {
         }  */
        console.log(req.file);

        return res.status(200).json({
            message: 'done'
        });
    }
};
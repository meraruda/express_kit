import express, { NextFunction, Request, Response } from 'express';

import { User } from './db';

import loginController from './controller/loginController';
import UserController from './controller/userController';
// import mailController from './controller/mailController';
// import loginController from './controller/loginController';
import multer from 'multer';

export const sessionRouter = express.Router();
export const router = express.Router();

const upload = multer({ storage: multer.memoryStorage() });

const checkLogin = (req: Request, res: Response, next: NextFunction) => {
  if (!req.user) return res.status(401).json({ message: 'Please log in' });
  if (!(req.user as User).valid) res.status(401).json({ message: 'Please verify your mail' });
  return next();
};

router.get('', (req, res) => res.send('hello world'));
// router.post('/user', )
sessionRouter.get('/user/statistic', checkLogin, UserController.getUserStatistic);
sessionRouter.get('/user', checkLogin, UserController.getUser);
sessionRouter.get('/users', checkLogin, UserController.getUserList);
sessionRouter.put('/user', checkLogin, UserController.updateUser);
sessionRouter.put('/user/password', checkLogin, UserController.resetPassword);
sessionRouter.put('/user/avatar', checkLogin, upload.single('avatar'), UserController.uploadAvatar);
sessionRouter.delete('/user', checkLogin, UserController.deleteUser);

// router.post('/mail', mailController.send);

sessionRouter.get('/login/mail', loginController.verifyMail);
router.post('/login/register', loginController.register);
router.post('/login/resent-mail', loginController.resentVerifyMail);
sessionRouter.post('/login', loginController.login);
sessionRouter.post('/logout', checkLogin, loginController.logout);
router.get('/login-google', loginController.loginWithGoogle);
sessionRouter.get('/signin-google', loginController.handleGoogleRedirect);
router.get('/login-facebook', loginController.loginWithFacebook);
sessionRouter.get('/signin-facebook', loginController.handleFaceBookRedirect);

router.get('/health', (req, res) => {
  // #swagger.ignore = true
  res.status(200).send('OK');
});

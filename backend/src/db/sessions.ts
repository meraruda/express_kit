import { Table, Column, Model, DataType, PrimaryKey, Default, AllowNull, Unique } from 'sequelize-typescript';

import { sequelize } from './db';

@Table
export class Sessions extends Model {
    @PrimaryKey    
    @Column(DataType.STRING)
    declare sid: string
    @Column(DataType.UUID)
    declare userId: string;        
    @Column(DataType.DATE)
    declare expires: Date;
    @Column(DataType.TEXT)
    declare data: string;    
}

sequelize.addModels([ Sessions ]);
import { Table, Column, Model, DataType, PrimaryKey, Default, AllowNull, Unique } from 'sequelize-typescript';
import { UUIDV4 } from 'sequelize';

import { sequelize } from './db';

@Table
export class User extends Model {
    @PrimaryKey
    @Default(UUIDV4)
    @Column(DataType.UUID)
    declare id: string
    @Column
    declare name?: string;
    @AllowNull(false)
    @Unique
    @Column
    declare mail: string;
    @Column
    declare password?: string;
    @Column
    declare token?: string;
    @Default(false)
    @Column
    declare valid: boolean;
}

sequelize.addModels([ User ]);
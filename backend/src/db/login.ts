import { Table, Column, Model, DataType, PrimaryKey, Default, AllowNull, Unique } from 'sequelize-typescript';

import { sequelize } from './db';

@Table
export class Login extends Model {
    @PrimaryKey    
    @Column(DataType.STRING)
    declare sid: string
    @Column(DataType.UUID)
    declare userId: string;          
}

sequelize.addModels([ Login ]);
import { Sequelize } from 'sequelize-typescript';
import config from '../config';

const sequelize = new Sequelize(config.dbUri, {
    dialect: "postgres",
    // dialectOptions: {
    //     ssl: {
    //         rejectUnauthorized: false
    //     }
    // }
});

export {
    sequelize
}
import dotenv from 'dotenv-flow';

dotenv.config();

export default {
  dbUri: process.env.DATABASE || '',
  mailServiceKey: process.env.SENDGRID_API_KEY || '',
  googleClientID: process.env.GOOGLE_API_ID || '',
  googleSecret: process.env.GOOGLE_API_SECRET || '',
  googleRedirect: process.env.GOOGLE_API_REDIRECT || '',
  host: process.env.HOST || '',
  mail: process.env.MAIL || '',
  frontend: process.env.FRONTEND_HOST || '',
  facebookAppID: process.env.FB_APP_ID || '',
  facebookSecret: process.env.FB_APP_SECRET || '',
  isDev: process.env.ENV !== 'prod',
};

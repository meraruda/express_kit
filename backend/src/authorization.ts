import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import bcryptjs from 'bcryptjs';

import { User } from './db';

passport.use(new LocalStrategy(async (username, password, callback) => {
  const user = await User.findOne({ where: { mail: username } });
  if (user) {
    if (user.password && bcryptjs.compareSync(password, user.password)) {
      return callback(null, user);
    } else {
      return callback(null, user, { message: 'wrong password' });
    }
  }

  return callback(null, false);
}));

passport.serializeUser((user, callback) => {
  callback(null, (user as User).id);
});

passport.deserializeUser(async (id, callback) => {
  try {
    const user = await User.findOne({ where: { id } });
    callback(null, user);
  } catch (e) {
    callback(e, false);
  }
});

export default passport;

import Express from 'express';
import { validationResult, ValidationChain } from 'express-validator';

export default (validations: ValidationChain[]) => async (
  req: Express.Request,
  res: Express.Response,
  next: Express.NextFunction,
) => {
  await Promise.all(validations.map((validation) => (validation.run(req))));

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  return res.status(400).json({ errors: errors.array() });
};

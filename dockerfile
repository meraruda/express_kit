FROM node:16-slim AS builder

COPY /frontend/ /frontend/

RUN cd frontend &&\
    npm install &&\
    npm run build



FROM nginx:alpine AS runtime

RUN apk update && apk add\
    nodejs\
    npm\ 
    bash

COPY /backend/ /api/
RUN cd api && npm install && npm run build
COPY /nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /frontend/build /usr/share/nginx/html/

ADD /nginx/start.sh /
RUN chmod +x /start.sh

CMD /start.sh
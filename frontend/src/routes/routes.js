import React, { Suspense, lazy } from 'react';
import { Switch, Route } from 'react-router-dom';

const Layout = lazy(() => import('../modules/Layout'));
const Login = lazy(() => import('../modules/Login/Login'));
const Home = lazy(() => import('../modules/Home/Home'));
const Three = lazy(() => import('../modules/Three/Three'));
const Track = lazy(() => import('../modules/Track/Track'));
const Dashboard = lazy(() => import('../modules/Dashboard/Dashboard'));
const Graph = lazy(() => import('../modules/Graph/Graph'));
const User = lazy(() => import('../modules/User/User'));

const WaitingComponent = Component => props => (
    <Suspense fallback={<div>Module loading....</div>}>
        <Component {...props} />
    </Suspense>
);

export const appPath = [
    { path: '/app/user', name: 'Profile', Comp: WaitingComponent(User) },
    { path: '/app/dashboard', name: 'Dashboard', Comp: WaitingComponent(Dashboard) },
    { path: '/app/three', name: 'ThreeJS', Comp: WaitingComponent(Three) },
    { path: '/app/graph', name: 'DataGraph', Comp: WaitingComponent(Graph) },
    { path: '/app/svg', name: 'TrackMap', Comp: WaitingComponent(Track) },
   { path: '/app', name: 'Home', Comp: WaitingComponent(Home) },
];

const APP = () => (
    <Layout>
        <Switch>
            {
                appPath.map(({ path, Comp }, index) => (<Route key={index} path={path} render={Comp} />))
            }
        </Switch>
    </Layout>
);

const routes = () => (
    <Switch>
        <Route path="/app" render={WaitingComponent(APP)} />
        <Route path="/three" render={WaitingComponent(Three)} />
        <Route path="/" render={WaitingComponent(Login)} />
    </Switch>
);

export default routes;

import React, { useState } from "react";
import { Statistic, Table, Row, Col, Skeleton } from "antd";
import { WifiOutlined } from "@ant-design/icons";

import useAxios from "../../hooks/useAxios";

import * as Style from "./style";
import useAsyncEffect from "../../hooks/useAsyncEffect";

const Dashboard = (props) => {
  const axios = useAxios();

  const [data, setData] = useState([]);
  const [statistic, setStatistic] = useState({});

  useAsyncEffect(async () => {
    const response = await axios.exec({
      url: "/users",
      method: "get",
    });

    setData(response);
  }, []);

  useAsyncEffect(async () => {
    const response = await axios.exec({
      url: "/user/statistic",
      method: "get",
    });

    setStatistic(response);
  }, []);

  const { total, active, week } = statistic;

  return (
    <Skeleton active loading={axios.isLoading}>
      <Row gutter={24}>
        <Col span={12}>
          <Statistic
            title="Current active session / Total user"
            value={active}
            suffix={`/${total}`}
          />
        </Col>
        <Col span={12}>
          <Statistic
            title="Average session past week"
            value={Math.floor(week / 7)}
            prefix={<WifiOutlined />}
          />
        </Col>
        <Col span={24}>
          <Table
            columns={[
              {
                title: "Name",
                dataIndex: "name",
              },
              {
                title: "Email",
                dataIndex: "mail",
              },
              {
                title: "Signup Time",
                dataIndex: "createdAt",
              },
              {
                title: "Login Times",
                dataIndex: "times",
              },
              {
                title: "Last Login Time",
                dataIndex: "last",
              },
            ]}
            dataSource={data}
            loading={axios.loading}
            pagination={false}
            style={{ width: "100%" }}
          />
        </Col>
      </Row>
    </Skeleton >
  );
};

export default Dashboard;

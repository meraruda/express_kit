import React, { useEffect } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import Stats from 'stats-js';
import Snow from './snow.png';

import { Container } from './style';

const Three = () => {
    const domId = 'canvas-container';

    useEffect(() => {
        // === Stats.JS ===
        const stats = new Stats();
        stats.showPanel(0);
        document.getElementById(domId).appendChild(stats.dom);

        // === THREE.JS CODE START ===
        const renderer = new THREE.WebGLRenderer();        
        renderer.setSize(window.innerWidth * .98, window.innerHeight * .8);
        renderer.setClearColor('#000', 1.0);
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = 2;
        document.getElementById(domId).appendChild(renderer.domElement);

        const scene = new THREE.Scene();
        scene.fog = new THREE.FogExp2('#333', 0.005);

        const camera = new THREE.PerspectiveCamera(
            100,
            window.innerWidth / window.innerHeight,
            0.5,
            1000
        );
        camera.position.set(30, 30, 20);
        camera.lookAt(scene.position);

        const cameraCtrl = new OrbitControls(camera, renderer.domElement);
        cameraCtrl.enableDamping = true;
        cameraCtrl.dampingFactor = 0.25;
        // cameraCtrl.autoRotate = true;

        const spotLight = new THREE.PointLight('#ffffff');
        spotLight.position.set(10, 10, 5);
        spotLight.castShadow = true;
        spotLight.power = 10;
        scene.add(spotLight);

        const moveLight = new THREE.PointLight('#ffffff', 1, 180);
        moveLight.castShadow = true;
        moveLight.power = 0.001;
        scene.add(moveLight);

        const sphereLightGeo = new THREE.SphereGeometry(1, 24, 24);
        const sphereLightMat = new THREE.MeshPhongMaterial({
            color: '#ffa500',
            metalness: 1,
            // wireframe: true,
        });
        const sphereLightMesh = new THREE.Mesh(sphereLightGeo, sphereLightMat);
        sphereLightMesh.castShadow = true;
        // sphereLightMesh.position.y = 16;
        // sphereLightMesh.position.x = 16;
        scene.add(sphereLightMesh);

        const light = new THREE.AmbientLight('#333');
        scene.add(light);

        // const axes = new THREE.AxesHelper(100);
        // scene.add(axes);

        // const headGeo = new THREE.BoxGeometry(5, 5, 5);
        // const bodyGeo = new THREE.BoxGeometry(4, 8, 2);
        // const footGeo = new THREE.BoxGeometry(2, 3, 2);

        // const mat = new THREE.MeshStandardMaterial({
        //     color: '#ff0000',
        //     // transparent: true,
        //     // opacity: 0.9,
        //     skinning: true,
        //     // wireframe: true,
        //     // wireframeLinewidth: 3, // useless
        //     // wireframeLinejoin: 'miter',
        //     roughness: 0.9,
        //     metalness: 0.2,
        //     // side: THREE.DoubleSide,
        //     // map: new THREE.TextureLoader().load(
        //     //     'https://dl.dropboxusercontent.com/s/bkqu0tty04epc46/creeper_face.png'
        //     // ),
        // });

        // const head = new THREE.Mesh(headGeo, mat);
        // head.position.set(0, 6, 0);
        // const body = new THREE.Mesh(bodyGeo, mat);
        // body.position.set(0, 0, 0);
        // const foot1 = new THREE.Mesh(footGeo, mat);
        // foot1.position.set(-1, -5.5, 2);
        // const foot2 = new THREE.Mesh(footGeo, mat);
        // foot2.position.set(-1, -5.5, -2);
        // const foot3 = new THREE.Mesh(footGeo, mat);
        // foot3.position.set(1, -5.5, 2);
        // const foot4 = new THREE.Mesh(footGeo, mat);
        // foot4.position.set(1, -5.5, -2);

        // const feet = new THREE.Group();
        // feet.add(foot1);
        // feet.add(foot2);
        // feet.add(foot3);
        // feet.add(foot4);

        // const creeper = new THREE.Group();
        // creeper.add(head);
        // creeper.add(body);
        // creeper.add(feet);
        // creeper.traverse((obj) => {
        //     if (obj instanceof THREE.Mesh) {
        //         obj.castShadow = true
        //         obj.receiveShadow = true
        //     }
        // });

        // scene.add(creeper);

        const ball = new THREE.SphereGeometry(10, 16, 16);
        const ballMat = new THREE.MeshBasicMaterial({
            color: '#4169e1',
            metalness: 0.5,
            wireframe: true,
        });
        const ballMesh = new THREE.Mesh(ball, ballMat);
        ballMesh.castShadow = true;
        ballMesh.position.set(0, 16, 0);
        scene.add(ballMesh);

        const ballPoint = new THREE.Points(
            new THREE.SphereGeometry(15, 50, 50),
            new THREE.PointsMaterial({
                color: '#00bfff',
                size: 0.1,
            }
            ));
        ballPoint.position.set(0, 16, 0);
        ballPoint.castShadow = true;
        scene.add(ballPoint);

        const planeGeo = new THREE.PlaneGeometry(200, 200);
        const planeMaterial = new THREE.MeshLambertMaterial({ color: '#ffffff' });
        const plane = new THREE.Mesh(planeGeo, planeMaterial);
        plane.rotation.x = -0.5 * Math.PI;
        plane.position.set(0, -6, 0);
        plane.receiveShadow = true;
        scene.add(plane);

        const galaxyRange = 150;
        const galaxyGeo = new THREE.Geometry()
        Array.from({ length: 60000 }).forEach(() => {
            const point = new THREE.Vector3(
                THREE.Math.randInt(-galaxyRange, galaxyRange),
                THREE.Math.randInt(-galaxyRange, galaxyRange),
                THREE.Math.randInt(-galaxyRange, galaxyRange),
            );
            point.velocityX = THREE.Math.randFloat(-0.16, 0.16)
            point.velocityY = THREE.Math.randFloat(0.1, 0.3)
            galaxyGeo.vertices.push(point);
        });
        scene.add(new THREE.Points(
            galaxyGeo,
            new THREE.PointsMaterial({
                size: 1,
                map: new THREE.TextureLoader().load(Snow),
                blending: THREE.AdditiveBlending,
                depthTest: true,
                transparent: true,
                opacity: 0.7
            }),
        ));

        let rotateAngle = 0;
        // let creeperRotate = 0;
        var animate = function () {
            requestAnimationFrame(animate);
            // creeperRotate += 0.2;
            // const movement = Math.sin(creeperRotate);
            // head.rotation.y = movement;
            // foot1.rotation.x = movement / 4;
            // foot2.rotation.x = -movement / 4;
            // foot3.rotation.x = -movement / 4;
            // foot4.rotation.x = movement / 4;
            // const scaleRate = Math.abs(Math.sin(creeperRotate)) / 16 + 1;
            // creeper.scale.set(scaleRate, scaleRate, scaleRate);

            // const scaleRate = Math.abs(Math.sin(rotateAngle)) / 16 + 1;
            // ballMesh.scale.set(scaleRate, scaleRate, scaleRate);
            // ballMesh.rotation.x = Math.cos(rotateAngle);
            ballMesh.rotation.x = 0.2;
            ballMesh.rotation.y = rotateAngle;

            ballPoint.rotation.set(rotateAngle, rotateAngle, rotateAngle);

            cameraCtrl.update();
            stats.update();

            if (rotateAngle > 2 * Math.PI) {
                rotateAngle = 0 // 超過 360 度後歸零
            } else {
                rotateAngle += 0.005 // 遞增角度
            }

            sphereLightMesh.rotation.x = 0.2;
            sphereLightMesh.rotation.y = rotateAngle * 10;
            // const scaleRate = Math.abs(Math.sin(rotateAngle)) + 1;
            // const scaleRate = Math.cos(rotateAngle);
            // sphereLightMesh.scale.set(scaleRate, scaleRate, scaleRate);

            // 光源延橢圓軌道繞 Y 軸旋轉
            sphereLightMesh.position.x = 20 * Math.cos(rotateAngle);
            sphereLightMesh.position.z = 30 * Math.sin(rotateAngle);
            sphereLightMesh.position.y = 16 + 10 * Math.sin(rotateAngle);

            // 點光源位置與球體同步
            moveLight.position.copy(sphereLightMesh.position);
            moveLight.position.y = 15 + 10 * Math.sin(rotateAngle);

            // 雪花落下循環動畫
            galaxyGeo.vertices.forEach((v) => {
                if (v.y >= -5.5) {
                    v.y = v.y - v.velocityY
                    v.x = v.x - v.velocityX
                }
                if (v.y <= -galaxyRange) v.y = galaxyRange
                if (v.x <= -galaxyRange || v.x >= galaxyRange) v.velocityX = v.velocityX * -1

            })
            galaxyGeo.verticesNeedUpdate = true

            renderer.render(scene, camera);
        };

        animate();
        // === THREE.JS EXAMPLE CODE END ===
    }, []);

    return (
        <Container id={domId} />
    );
}

export default Three;
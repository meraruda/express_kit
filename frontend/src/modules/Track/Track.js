import React, { useEffect, useState } from "react";

import { Progress } from "antd";

import Train from "./train.svg";
import * as Style from "./style";

export default (props) => {
  const [tracks] = useState([
    { start: [30, 200], end: [530, 200] },
    { start: [540, 200], end: [1230, 200] },
    { start: [1240, 200], end: [1840, 200] },
  ]);
  const [trainPos, setTrainPos] = useState([30, 200]);
  const [trainSpd, setTrainSpd] = useState([0]);
  const STEP = 30;

  useEffect(() => {
    const id = setInterval(() => {
      setTrainPos((pre) => [pre[0] + STEP > 1840 ? 30 : pre[0] + STEP, 200]);
      setTrainSpd(Math.floor(40 + Math.random() * 10));
    }, 1000);

    return () => {
      clearInterval(id);
    };
  }, []);

  const checkSectionBlock = (start, end) => {
    // Calculate the distance between points A and B
    const distanceAB = Math.sqrt(
      (end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2
    );

    // Calculate the distances from A to C and from B to C
    const distanceAC = Math.sqrt(
      (trainPos[0] - start[0]) ** 2 + (trainPos[1] - start[1]) ** 2
    );
    const distanceBC = Math.sqrt(
      (trainPos[0] - end[0]) ** 2 + (trainPos[1] - end[1]) ** 2
    );

    // Check if the sum of distances from A to C and from B to C is equal to the distance between A and B
    return Math.abs(distanceAC + distanceBC - distanceAB) < 0.0001
      ? "red"
      : "green";
  };

  return (
    <Style.Grid>
      <Style.Container>
        <text x="10" y="20">
          If one train running in a section, then this section should be block
          and others could not enter this section.
        </text>
        {tracks.map(({ start, end }) => (
          <line
            x1={start[0]}
            y1={start[1]}
            x2={end[0]}
            y2={end[1]}
            stroke={checkSectionBlock(start, end)}
          />
        ))}
        <image href={Train} x={trainPos[0]} y={trainPos[1] - 70} />
      </Style.Container>
      <Style.Panel>
        <Progress
          size={250}
          type="dashboard"
          percent={trainSpd}
          format={(num) => num + " km/h"}
          strokeColor={{
            "0%": "#87d068",
            "50%": "#ffe58f",
            "100%": "#dc143c",
          }}
          strokeWidth={8}
          strokeLinecap="square"
          trailColor="#808080"
        />
      </Style.Panel>
      <Style.Panel>
        <iframe
          src="https://www.youtube.com/embed/7YeLuTL0qzk?playlist=7YeLuTL0qzk&disablekb=0&version=3&loop=1&fs=0&controls=0&mute=1&autoplay=1"
          title="YouTube video player"
          frameborder="0"
        />
      </Style.Panel>
    </Style.Grid>
  );
};

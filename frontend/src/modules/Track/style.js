import styled from 'styled-components';

export const Container = styled.svg`
  grid-column-start: 1;
  grid-column-end: 3;

  width: 100%;
  height: 400px;

  > line {
    stroke-width: 20px;
  }

  > image {
    width: 50px;
    height: 60px;

    filter: invert(0.5);
  }

  > text {
    font-size: 20px;
    font-weight: bold;

    fill: #fff;
  }
`;

export const Grid = styled.div`
  width: 100%;

  background: #000;
  overflow: hidden;

  display: grid;
  grid-template-columns: 50% 50%;
`;

export const Panel = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  > iframe {
    margin: 10px 20px;

    width: 100%;
    height: 100%;
  }

  .ant-progress-text {
    color: #fff !important;
  }

`;

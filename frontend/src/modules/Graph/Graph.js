import React, { useEffect, useRef } from "react";
import { DataSet } from "vis-data/peer";
import { Network } from "vis-network";

import data from './data.json';
import * as Style from "./style";

export default (props) => {
  const graph = useRef(null);

  useEffect(() => {    
    const network = new Network(graph.current, JSON.parse(data), {
      nodes: {
        shape: "dot",
        size: 16,
      },
      physics: {
        forceAtlas2Based: {
          gravitationalConstant: -26,
          centralGravity: 0.005,
          springLength: 230,
          springConstant: 0.18,
        },
        maxVelocity: 146,
        solver: "forceAtlas2Based",
        timestep: 0.35,
        stabilization: { iterations: 150 },
      },
    });
  }, [graph]);

  return (
    <>
      <Style.Container id="graph-canvas" ref={graph} />
    </>
  );
};

import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { Button, Form, Input, Skeleton } from "antd";

import useAxios from "../../hooks/useAxios";
import useAsyncEffect from "../../hooks/useAsyncEffect";
import ResetPassword from "./ResetPassword";

import * as Style from "./style";

const User = (props) => {
  const axios = useAxios();
  const [form] = Form.useForm();
  const [initValues, setInitValues] = useState();

  useAsyncEffect(async () => {
    const values = await axios.exec({
      url: "/user",
      method: "get",
    });

    setInitValues(values);
    form.resetFields();
  }, []);

  const onFinish = async ({ name }) => {
    await axios.exec({
      url: "/user",
      method: "put",
      data: {
        name,
      }
    });
  };

  return (
    <Skeleton active loading={axios.isLoading}>
      <Form
        form={form}
        name="register"
        onFinish={onFinish}
        initialValues={initValues}
        style={{ maxWidth: 600 }}
        scrollToFirstError
      >
        <Form.Item
          name="mail"
          label="E-mail"
          rules={[
            {
              type: "email",
              message: "The input is not valid E-mail!",
            },
            {
              required: true,
              message: "Please input your E-mail!",
            },
          ]}
        >
          <strong>{initValues?.mail}</strong>
        </Form.Item>
        <Form.Item
          name="name"
          label="Name"
          rules={[
            {
              required: true,
              message: "Please input your name!",
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={axios.isLoading}>
            Save
          </Button>
          &nbsp;
          <ResetPassword />
        </Form.Item>
      </Form>
    </Skeleton>
  );
};

export default withRouter(User);

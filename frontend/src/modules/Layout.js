import React, { useEffect } from "react";
import { Layout as LayoutComponent, Menu, Tooltip, Button } from "antd";
import { Link, useHistory } from "react-router-dom";
import { LogoutOutlined } from "@ant-design/icons";

import { appPath } from "../routes/routes";
import useSession from "../hooks/useSession";
import useAxios from "../hooks/useAxios";

import * as Style from "./style";

const { Header, Content, Footer, Sider } = LayoutComponent;

const Layout = (props) => {
  const axios = useAxios();
  const history = useHistory();
  const { sessionId, removeSession } = useSession();

  if (!sessionId) {
    history.push("/");
  }

  const logout = async () => {
    await axios.exec({
      url: "/logout",
      method: "post",
    });
    removeSession();
    history.push("/");
  };

  return (
    <Style.Container id="layout">
      <LayoutComponent>
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            selectable={false}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item>
              <Tooltip title="Logout">
                <Button
                  shape="circle"
                  icon={<LogoutOutlined />}
                  onClick={logout}
                />
              </Tooltip>
            </Menu.Item>
            {appPath.map(({ name, path }, index) => (
              <Menu.Item key={index}>
                <Link to={{ pathname: path }}>{name}</Link>
              </Menu.Item>
            ))}
          </Menu>
        </Header>
        <Content>
          <Style.ChildContainer>{props.children}</Style.ChildContainer>
        </Content>
        <Footer>©2018 Created</Footer>
      </LayoutComponent>
    </Style.Container>
  );
};

export default Layout;

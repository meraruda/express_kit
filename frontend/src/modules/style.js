import styled from 'styled-components';

export const Container = styled.div`
    height: 100vh;

    .ant-layout {
        height: 100%;
    }
`;

export const ChildContainer = styled.div`
    display: flex;
    justify-content: center;
    height: 100%;
    background: #fff;
    padding: 24px;
`;

import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button } from 'antd';
import * as Style from './style';

const Home = (props) => {
    const handleClick = () => {
        const { history } = props;
        history.push('app/test');
    }

    return (
        <Style.MapArea>
            <iframe src={`https://www.google.com/maps/embed/v1/view?key=${process.env.REACT_APP_MAP_API_KEY}&center=25.03488,121.5625219&zoom=16`} allowFullScreen></iframe>
        </Style.MapArea>
    );
}

export default withRouter(Home);
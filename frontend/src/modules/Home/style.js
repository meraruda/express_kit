import styled from 'styled-components';

export const MapArea = styled.div`
    width: 100%;
    height: 100%;

    iframe {
        width: 100%;
        height: 100%;
        border: none;
    }
`;
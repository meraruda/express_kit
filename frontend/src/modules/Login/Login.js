import React, { useEffect, useState } from "react";
import { Button, Icon, Form, Input } from "antd";
import {
  GoogleOutlined,
  FacebookOutlined,
  MailOutlined,
  KeyOutlined,
} from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";

import useAxios from "../../hooks/useAxios";
import useSession from "../../hooks/useSession";

import Register from "./Register";
import Resent from "./Resent";
import * as Style from "./style";

const Login = (props) => {
  const axios = useAxios();
  const history = useHistory();

  const { sessionId } = useSession();

  if (sessionId) {
    history.push("/app");
  }

  const [isResentModalOpen, setIsResentModalOpen] = useState(false);

  const handleGoogleLogin = async () => {
    const { url } = await axios.exec({
      url: "/login-google",
      method: "get",
    });
    window.location.href = url;
  };

  const handleFacebookLogin = async () => {
    const { url } = await axios.exec({
      url: "/login-facebook",
      method: "get",
    });
    window.location.href = url;
  };

  const onFinish = async (values) => {
    console.log("Success:", values);
    try {
      await axios.exec({
        url: "/login",
        method: "post",
        data: {
          ...values,
        },
      });

      return false;
    } catch (e) {
      if (e.response.status == 307) {
        setIsResentModalOpen(values);
      }
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
    <Style.Background />
    <Style.Background className="reverse" duration="4s"/>
    <Style.Background duration="5s"/>
    <Style.LoginContainer>
      <Resent
        isModalOpen={isResentModalOpen}
        setIsModalOpen={setIsResentModalOpen}
      />
      <Style.LoginForm>
        <Form
          name="basic"
          labelCol={{
            span: 0,
          }}
          wrapperCol={{
            span: 24,
          }}
          style={{
            maxWidth: 600,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your mail!",
              },
            ]}
          >
            <Input prefix={<MailOutlined />} placeholder="Mail" />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password prefix={<KeyOutlined />} placeholder="Password" />
          </Form.Item>
          <Form.Item wrapperCol={{ span: 24 }}>
            <Button loading={axios.isLoading} htmlType="submit" type="primary">
              Submit
            </Button>
            <Register/>
            <Button
              onClick={handleGoogleLogin}
              loading={axios.isLoading}
              type="primary"
            >
              <GoogleOutlined />
              Login with Google
            </Button>
            <Button
              onClick={handleFacebookLogin}
              loading={axios.isLoading}
              type="primary"
            >
              <FacebookOutlined />
              Login with Facebook
            </Button>
          </Form.Item>
        </Form>
      </Style.LoginForm>
    </Style.LoginContainer>
    </>
  );
};

export default Login;

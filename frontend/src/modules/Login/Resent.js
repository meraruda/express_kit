import React from "react";
import { Button, Modal, Form, Input, message } from "antd";

import useAxios from "../../hooks/useAxios";

export default ({ isModalOpen, setIsModalOpen }) => {
  const axios = useAxios();
  const [messageApi, contextHolder] = message.useMessage();

  const { username } = isModalOpen;

  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const onResentClick = async () => {
    await axios.exec({
      url: "/login/resent-mail",
      method: "post",      
      data: {
        mail: username
      }
    });
    messageApi.success("Please check your mailbox for confirmation");
    setIsModalOpen(false);
  };

  return (
    <>
      {contextHolder}
      <Modal
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        closeIcon={null}
        footer={null}
        bodyStyle={{
          height: "30vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Button type="primary" onClick={onResentClick} loading={axios.isLoading}>
          Resend Email Verification
        </Button>
      </Modal>
    </>
  );
};

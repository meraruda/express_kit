import React, { useState } from "react";
import { Button, Modal, Form, Input, message } from "antd";

import useAxios from "../../hooks/useAxios";

export default () => {
  const axios = useAxios();
  const [messageApi, contextHolder] = message.useMessage();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);    
  };
  const handleOk = () => {
    form.submit();
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const [form] = Form.useForm();
  const [initValues, setInitValues] = useState();

  const onFinish = async ({ password, username, mail }) => {
    await axios.exec({
      url: "/login/register",
      method: "post",
      data: {
        password,
        username,
        mail,
      },
    });
    messageApi.success("Please check your mailbox for confirmation");
    setIsModalOpen(false);
  };

  return (
    <>      
      {contextHolder}
      <Button loading={axios.isLoading} onClick={showModal} type="primary" >Sign up</Button>
      <Modal
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Submit"
        closeIcon={null}
        confirmLoading={axios.isLoading}
      >
        <Form
          labelCol={{ span: 10 }}
          form={form}
          name="register"
          onFinish={onFinish}
          initialValues={initValues}
          style={{ maxWidth: 600 }}
          scrollToFirstError
        >
          <Form.Item
            name="mail"
            label="E-mail"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="username"
            label="Name"
            rules={[
              {
                required: true,
                message: "Please input your name!",
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
              {
                pattern:
                  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{}[\]|;:'",<.>/?]).{8,}$/,
                message: "Password not valid!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error("The new password that you entered do not match!")
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          {/* <Form.Item wrapperCol={{ offset: 20 }}>
            <Button type="primary" htmlType="submit">
              Reset
            </Button>
          </Form.Item> */}
        </Form>
      </Modal>
    </>
  );
};

import styled, { keyframes } from 'styled-components';

export const LoginContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  // background-color: #4169e1;
`;

export const LoginForm = styled.div`
  width: 420px;
  background-color: #fff;
  border-radius: 3px;
  padding: 60px 30px 30px 30px;
  display: flex;
  flex-direction: column;
  align-items: center;

  button {
    width: 100%;
    margin-top: 6px;
  }

  button, input {
    height: 40px;
  }

  span.ant-input-prefix {
    padding-right: 6px;
  }
`;

const Frames = keyframes`
  0% {
    transform:translateX(-25%);
  }
  100% {
    transform:translateX(25%);
  }
`;

export const Background = styled.div`
  animation:${Frames} 3s ease-in-out infinite alternate;
  background-image: linear-gradient(-45deg, #4169e1 50%, #09f 50%);
  bottom:0;
  left:-50%;
  opacity:.5;
  position:fixed;
  right:-50%;
  top:0;
  z-index:-1;
  animation-duration: ${props => props.duration || 0};

  &.reverse {
    animation-direction:alternate-reverse; 
  }
`

import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { ConfigProvider } from "antd";
import { Router } from "react-router-dom";
import { hot } from "react-hot-loader/root";
import { createHashHistory } from "history";
import Routes from "./routes/routes";

const history = createHashHistory();

const App = () => {
  return (
    <div>
      <ConfigProvider
        theme={{
          token: {
            colorPrimary: "#4169e1",
            borderRadius: 0,
          },
          components: {
            Button: {
              colorPrimary: "#4169e1",
              borderRadius: 0,
            },
          },
        }}
      >
        <Router history={history}>
          <Routes />
        </Router>
      </ConfigProvider>
    </div>
  );
};

export default hot(App);

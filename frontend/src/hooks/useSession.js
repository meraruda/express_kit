import Cookie from 'js-cookie';

export default () => {
    const sessionId = Cookie.get('connect.sid');    
    const removeSession = () => Cookie.remove('connect.sid');

    return {
        sessionId,
        removeSession,
    };
}
import { useState } from "react";
import { message } from 'antd';
import { useHistory } from "react-router-dom";
import Axios from 'axios';

import useSession from "./useSession";

const useAxios = (defaultResponse = {}) => {
    const { removeSession } = useSession();
    const history = useHistory();

    const config = {
        baseURL: process.env.REACT_APP_API_BASE_URI || `${window.location.protocol}//${window.location.hostname}/api`,
        withCredentials: true
    };

    if (sessionStorage.accessToken) {
        config.headers = {
            Authorization: sessionStorage.accessToken,
        };
    }

    const axios = Axios.create(config);
    const [isLoading, setIsLoading] = useState(false);
    const [response, setResponse] = useState(defaultResponse);
    const [error, setError] = useState();

    const exec = async (config = {}) => {
        let responseData = defaultResponse;
        setIsLoading(true);
        try {
            const { data } = await axios(config);
            responseData = data;
            setResponse(responseData);
            return responseData;
        } catch (e) {      
            setError(e);

            if (e?.response?.status == 401) {
                removeSession();
                // window.location.href = '/'
                history.push('/')
            }

            if (e.response?.data) {                
                message.error(e.response.data?.message || e.response.data.status);
            } else {
                message.error(e.message || JSON.stringify(e));
            }        
            throw e;
        } finally {
            setIsLoading(false);
        }
    };

    return ({
        isLoading,
        response,
        exec,
        error,
    });
};

export default useAxios;

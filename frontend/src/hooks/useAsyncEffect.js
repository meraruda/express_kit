import { useEffect } from 'react';

// effect: () => Promise<void | (() => void)>, dependencies?: any[]
export default (effect, dependencies) => {
    return useEffect(() => {
        const cleanupPromise = effect()
        return () => { cleanupPromise.then(cleanup => cleanup && cleanup()) }
    }, dependencies)
}